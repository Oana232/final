import requests
import json
import os

URL = "https://gitlab.com/api/v4/projects"
ID  = "/14834469/"
X = "repository/commits"

r = requests.get(url = URL+ID+X)

data = {}

for commit in r.json():
    if data.get(commit['author_name']):
        data[commit['author_name']] = data[commit['author_name']] + 1
    else:
        data[commit['author_name']] = 1
print(data)


